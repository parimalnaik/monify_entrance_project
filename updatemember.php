<?php
/**
   * Update member
   * 
   * update selected member detail
   * 
   * @author  Parimal Naik
   */

require 'class.crud.php';
$obj = new CRUD();
//prepare array of column values
$data = array('id' => $_POST['id'],
                'firstname' => $_POST['firstname'],
                'lastname' => $_POST['lastname'],
                'email' => $_POST['email'],
                'telephone' => $_POST['phone'],
                'dob' => $_POST['dob'],
                'address' => $_POST['address'],
                'province' => $_POST['province'],
                'city' => $_POST['city'],
                'postalcode' => $_POST['postalcode']);

$result = $obj->update_member($data);
echo json_encode(array("status" => TRUE));
?>