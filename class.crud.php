<?php
/**
   * CRUD class
   * 
   * Handles the databse operations using PDO prepared statements
   * 
   * @author  Parimal Naik
   */
require 'dbconfig.php'; // database configuration file
class CRUD
{
    protected $db;
    var $column_order = array(null,'firstName','lastName','email','dob','city','province',null); //column array for sorting in datatable 
    var $column_order_del = array(null,'firstName','lastName','email','dob','city','province'); //column array for sorting in datatable
    function __construct()
    {
        $this->db = DB();
    }

    function __destruct()
    {
        $this->db = null;
    }

    /* function memberlist
     *
     * Load all records
     * @param none
     * @return result set of selected records
     * */
    public function membersList()
    {
        if(isset($_POST['order'])) // set order by based on selection in datatable or else in default order
        {
            $order_by = $this->column_order[$_POST['order']['0']['column']]; //order by column from column array defined in beginning
            $in_order = $_POST['order']['0']['dir']; //in which order ASC or DESC
        } 
        else
        {
            $order_by = "id";
            $in_order = "DESC";
        }

        //set the limit with number of rows & starting 
        $start=intval(@$_POST['length']);
        $per_page=intval(@$_POST['start']);

        $query = $this->db->prepare("SELECT id,firstname,lastname,email,dob,city,province FROM members where isdeleted=0 ORDER BY ".$order_by ." ".$in_order. " LIMIT ".$start." OFFSET ".$per_page);
        $query->execute();
        //$data = array();
        // while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
        //     $data[] = $row;
        // }
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }
     /* function count_all
     *
     * count total records
     * @param none
     * @return count of records
     * */
    public function count_all()
    {
        $result = $this->db->prepare("SELECT count(id) FROM members WHERE isdeleted=0"); 
        $result->execute();
        $rowCount = $result->fetchColumn(0);
        return $rowCount;
    }
     /* function count_selected
     *
     * count total selected records
     * @param none
     * @return count of records
     * */
    public function count_selected()
    {
        //set the limit with number of rows & starting 
        $start=intval(@$_POST['length']);
        $per_page=intval(@$_POST['start']);

        $result = $this->db->prepare("SELECT count(id) FROM members where isdeleted=0 LIMIT ".$start." OFFSET ".$per_page);
        $result->execute();
        $rowCount = $result->fetchColumn(0);
        return $rowCount;
    }
     /* function deleted_members_list
     *
     * Load all deleted records
     * @param none
     * @return result set of selected records
     * */
    public function deleted_members_List()
    {
        if(isset($_POST['order'])) // set order by based on selection in datatable or else in default order
        {
            $order_by = $this->column_order_del[$_POST['order']['0']['column']]; //order by column from column array defined in beginning
            $in_order = $_POST['order']['0']['dir']; //in which order ASC or DESC
        } 
        else
        {
            $order_by = "id";
            $in_order = "DESC";
        }

        //set the limit with number of rows & starting 
        $start=intval(@$_POST['length']);
        $per_page=intval(@$_POST['start']);

        $query = $this->db->prepare("SELECT id,firstname,lastname,email,dob,city,province FROM members where isdeleted=1 ORDER BY ".$order_by ." ".$in_order. " LIMIT ".$start." OFFSET ".$per_page);
        $query->execute();
        //$data = array();
        // while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
        //     $data[] = $row;
        // }
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }
    /* function del_count_all
     *
     * count total deleted records
     * @param none
     * @return count of records
     * */
    public function del_count_all()
    {
        $result = $this->db->prepare("SELECT count(id) FROM members WHERE isdeleted=1"); 
        $result->execute();
        $rowCount = $result->fetchColumn(0);
        return $rowCount;
    }
    /* function del_count_selected
     *
     * count selected deleted records
     * @param none
     * @return count of records
     * */
    public function del_count_selected()
    {
        //set the limit with number of rows & starting 
        $start=intval(@$_POST['length']);
        $per_page=intval(@$_POST['start']);

        $result = $this->db->prepare("SELECT count(id) FROM members WHERE isdeleted=1 LIMIT ".$start." OFFSET ".$per_page);
        $result->execute();
        $rowCount = $result->fetchColumn(0);
        return $rowCount;
    }
     /* function add_member
     *
     * add new member in to database table
     * @param array of column values
     * @return last insert id
     * */
    public function add_member($data)
    {   
        $firstname=$this->sanitizeinput($data['firstname']);
        $lastname=$this->sanitizeinput($data['lastname']);
        $email=$this->sanitizeinput($data['email']);
        $telephone=$this->sanitizeinput($data['telephone']);
        $dob=$this->sanitizeinput($data['dob']);
        $address=$this->sanitizeinput($data['address']);
        $city=$this->sanitizeinput($data['city']);
        $province=$this->sanitizeinput($data['province']);
        $postalcode=$this->sanitizeinput($data['postalcode']);

        $query = $this->db->prepare("INSERT INTO members(firstname, lastname, email, telephone, dob, address, city, province, postalcode) VALUES (:firstname,:lastname,:email,:telephone,:dob,:address,:city,:province,:postalcode)");

        $query->bindParam("firstname", $firstname, PDO::PARAM_STR);
        $query->bindParam("lastname", $lastname, PDO::PARAM_STR);
        $query->bindParam("email", $email, PDO::PARAM_STR);
        $query->bindParam("telephone", $telephone, PDO::PARAM_STR);
        $query->bindParam("dob", $dob, PDO::PARAM_STR);
        $query->bindParam("address", $address, PDO::PARAM_STR);
        $query->bindParam("city", $city, PDO::PARAM_STR);
        $query->bindParam("province", $province, PDO::PARAM_STR);
        $query->bindParam("postalcode", $postalcode, PDO::PARAM_STR);

        $query->execute();
        return $this->db->lastInsertId();
    }
     /* function get_member_detail
     *
     * get detail of selected member
     * @param id
     * @return member detail
     * */
    public function get_member_detail($id)
    {
        $query = $this->db->prepare("SELECT * FROM members WHERE id = :id");
        $query->bindParam("id", $id, PDO::PARAM_INT);
        $query->execute();
        return json_encode($query->fetch(PDO::FETCH_ASSOC));
    }
     /* function update_member
     *
     * edit selected member record
     * @param array of column values
     * @return none
     * */
    public function update_member($data)
    {
        $firstname=$this->sanitizeinput($data['firstname']);
        $lastname=$this->sanitizeinput($data['lastname']);
        $email=$this->sanitizeinput($data['email']);
        $telephone=$this->sanitizeinput($data['telephone']);
        $dob=$this->sanitizeinput($data['dob']);
        $address=$this->sanitizeinput($data['address']);
        $city=$this->sanitizeinput($data['city']);
        $province=$this->sanitizeinput($data['province']);
        $postalcode=$this->sanitizeinput($data['postalcode']);

        $query = $this->db->prepare("UPDATE members SET firstname=:firstname, lastname=:lastname, email=:email, telephone=:telephone, dob=:dob, address=:address, city=:city, province=:province, postalcode=:postalcode WHERE id = :id");

        $query->bindParam("id", $data['id'], PDO::PARAM_INT);
        $query->bindParam("firstname", $firstname, PDO::PARAM_STR);
        $query->bindParam("lastname", $lastname, PDO::PARAM_STR);
        $query->bindParam("email", $email, PDO::PARAM_STR);
        $query->bindParam("telephone", $telephone, PDO::PARAM_STR);
        $query->bindParam("dob", $dob, PDO::PARAM_STR);
        $query->bindParam("address", $address, PDO::PARAM_STR);
        $query->bindParam("city", $city, PDO::PARAM_STR);
        $query->bindParam("province", $province, PDO::PARAM_STR);
        $query->bindParam("postalcode", $postalcode, PDO::PARAM_STR);

        $query->execute();
    }
     /* function delete_member
     *
     * Delete selected member
     * @param id
     * @return none
     * */
    public function delete_member($id)
    {
        $memdeleted=1;
        $query = $this->db->prepare("UPDATE members SET isdeleted = :memdeleted WHERE id = :id");
        $query->bindParam("memdeleted", $memdeleted, PDO::PARAM_INT);
        $query->bindParam("id", $id, PDO::PARAM_INT);
        $query->execute();
    }
     /* function batchInsertMembers
     *
     * Insert batch data for xmlparser
     * @param array of column values
     * @return none
     * */
    public function batchInsertMembers($members)
    {
        $sql = "insert into members (id, firstname, lastname, email, telephone, dob, address, city, province, postalcode) values ";

        $paramArray = array();
        $sqlArray = array();

        foreach($members as $row)
        {
            // var_dump($row);exit();
            $sqlArray[] = '(' . implode(',', array_fill(0, count($row), '?')) . ')';

            foreach($row as $element)
            {
                $paramArray[] = $element;
            }
        }

        $sql .= implode(',', $sqlArray);

        $stmt = $this->db->prepare($sql);
        // print_r($stmt);exit();
        $stmt->execute($paramArray);
    }
     /* function sanitizeinput
     *
     * clean the input values before inserting into databse
     * @param input values
     * @return cleaned input value
     * */
    private function sanitizeinput($input) 
    {
        if (get_magic_quotes_gpc()) 
        {
            $input = stripslashes($input);
        }
        $search = array('@<script[^>]*?>.*?</script>@si',   // Strip out javascript
                        '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
                        '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
                        '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
                        );
 
        $output = preg_replace($search, '', $input);
        return trim($output);
    } 
}
?>