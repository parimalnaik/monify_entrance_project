<?php include_once 'header.php'; ?>
    <div class="row">
        <div class="col-md-12">
            <h3>Members Deleted From List:</h3>
            <table id="delmemberstable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>DOB</th>
                    <th>City</th>
                    <th>Province</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        </div>
    </div>
<?php include_once 'footer.php';?>