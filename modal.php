<div class="modal fade" id="addMember_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="addMemberForm" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                        <label class="control-label col-md-3">First Name</label>
                            <div class="col-md-9">
                                <input name="firstname" placeholder="First Name" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="control-label col-md-3">Last Name</label>
                            <div class="col-md-9">
                                <input name="lastname" placeholder="Last Name" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="control-label col-md-3">Email</label>
                            <div class="col-md-9">
                                <input name="email" placeholder="Email" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="control-label col-md-3">Phone</label>
                            <div class="col-md-9">
                                <input name="phone" placeholder="Phone" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="control-label col-md-3">DOB</label>
                            <div class="col-md-9">
                                <input name="dob" placeholder="DOB" class="form-control"  id="datepicker" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="control-label col-md-3">Address</label>
                            <div class="col-md-9">
                                <input name="address" placeholder="Address" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="control-label col-md-3">City</label>
                            <div class="col-md-9">
                                <input name="city" placeholder="City" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="control-label col-md-3">Province</label>
                            <div class="col-md-9">
                                <input name="province" placeholder="Province" maxlength="2" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="control-label col-md-3">Postal Code</label>
                            <div class="col-md-9">
                                <input name="postalcode" placeholder="Postalcode" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_member()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="viewMember" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <div class="modal-body form"> 
                    <div class="form-body">
                        <div class="form-group">
                        <label class="control-label col-md-3">First Name</label>
                            <div class="col-md-9">
                                <label name="firstname" class="form-control"></label>
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="control-label col-md-3">Last Name</label>
                            <div class="col-md-9">
                                 <label name="lastname" class="form-control"></label>
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="control-label col-md-3">Email</label>
                            <div class="col-md-9">
                                <label name="email" class="form-control"></label>
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="control-label col-md-3">Phone</label>
                            <div class="col-md-9">
                                 <label name="phone" class="form-control"></label>
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="control-label col-md-3">DOB</label>
                            <div class="col-md-9">
                                 <label name="dob" class="form-control"></label>
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="control-label col-md-3">Address</label>
                            <div class="col-md-9">
                                 <label name="address" class="form-control"></label>
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="control-label col-md-3">City</label>
                            <div class="col-md-9">
                                 <label name="city" class="form-control"></label>
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="control-label col-md-3">Province</label>
                            <div class="col-md-9">
                                 <label name="province" class="form-control"></label>
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="control-label col-md-3">Postal Code</label>
                            <div class="col-md-9">
                                 <label name="postalcode" class="form-control"></label>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->