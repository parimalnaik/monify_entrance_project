<?php
require 'class.crud.php';
$obj = new CRUD();

//prepare array of columns
$data = array('firstname' => $_POST['firstname'],
                'lastname' => $_POST['lastname'],
                'email' => $_POST['email'],
                'telephone' => $_POST['phone'],
                'dob' => $_POST['dob'],
                'address' => $_POST['address'],
                'province' => $_POST['province'],
                'city' => $_POST['city'],
                'postalcode' => $_POST['postalcode']);

$result = $obj->add_member($data); //add member detail into table
if(isset($result) && $result > 0)
{
	echo json_encode(array("status" => TRUE)); // return JSON success response
}
?>