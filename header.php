<?php define("BASEURL","http://localhost/crud/");?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Monify Media CRUD example</title>

    <!-- Bootstrap CSS File  -->
    <link rel="stylesheet" type="text/css" href="<?php echo BASEURL;?>bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo BASEURL;?>datatables/css/dataTables.bootstrap.css"/>
     <link rel="stylesheet" type="text/css" href="<?php echo BASEURL;?>datepicker/jquery-ui.css"/>
    
    <!-- Jquery JS file -->
    <script type="text/javascript" src="<?php echo BASEURL;?>js/jquery-3.2.1.min.js"></script>

	<!-- Bootstrap JS file -->
	<script type="text/javascript" src="<?php echo BASEURL;?>bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo BASEURL;?>datatables/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?php echo BASEURL;?>datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo BASEURL;?>datepicker/jquery-ui.js"></script>
    
    <!-- toastr JS/CSS file -->
    <script type="text/javascript" src="<?php echo BASEURL;?>toastr/js/toastr.min.js"></script>
    <link href="<?php echo BASEURL;?>toastr/css/toastr.min.css" rel="stylesheet">

    <!-- Custom JS file for AJAX functions-->
    <script type="text/javascript" src="<?php echo BASEURL;?>js/commonfunctions.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Monify Media - CRUD example</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
               <a href="#" id="btnhome" class="btn btn-info">
          <span class="glyphicon glyphicon-home"></span> Home
        </a>&nbsp;<button id="btndeleted" class="btn btn-info">View Deleted Members</button>&nbsp;<button class="btn btn-info" onclick="open_new_modal()">Add New Member</button>
            </div>
        </div>
    </div>