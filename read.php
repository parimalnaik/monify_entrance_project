<?php
/**
   * list all member
   * 
   * list all the members in datatable which are not logically deleted
   * 
   * @author  Parimal Naik
   */
require 'class.crud.php';

$object = new CRUD();

$member = $object->membersList();

$data = array();
$no = @$_POST['start'];
foreach ($member as $members) {
    $no++;
    $row = array();
    $row[] = $no;
    $row[] = $members['firstname'];
    $row[] = $members['lastname'];
    $row[] = $members['email'];
    $row[] = $members['dob'];
    $row[] = $members['city'];
    $row[] = $members['province'];
    $row[] = '<div align="center"><a class="btn btn-success btn-sm" href="javascript:void(0)" title="View" onclick="view_member('."'".$members['id']."'".')"><i class="glyphicon glyphicon-eye-open"></i></a>&nbsp;&nbsp;<a class="btn btn-warning btn-sm" href="javascript:void(0)" title="Edit" onclick="edit_member('."'".$members['id']."'".')"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;&nbsp;<a class="btn btn-danger btn-sm" href="javascript:void(0)" title="Hapus" onclick="delete_member('."'".$members['id']."'".')"><i class="glyphicon glyphicon-trash"></i></a></div>';

    $data[] = $row;
}

$output = array(
                "draw" => @$_POST['draw'],
                "recordsTotal" => $object->count_all(),
                "recordsFiltered" => $object->count_selected(),
                "data" => $data,
        );
//output to json format
echo json_encode($output);
?>