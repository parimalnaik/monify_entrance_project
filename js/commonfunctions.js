var table;
var deltable;
var save_mode;
var approot = "http://localhost/crud/";
$(document).ready(function () {

$( "#datepicker" ).datepicker({
  dateFormat: "yy-mm-dd"
});
    //configuration for success message popup
    toastr.options = {
    "debug": false,
    "positionClass": "toast-top-center",
    "onclick": null,
    "preventDuplicates": true,
    "fadeIn": 300,
    "fadeOut": 1000,
    "timeOut": 5000,
    "extendedTimeOut": 1000
    }

    //datatable configuration
    table = $('#memberstable').DataTable({ 
        "processing": true,
        "serverSide": true,
        "searching": false,
        "order": [],
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "read.php",
            "type": "POST",
            data:{},
            complete: function () {
                //alert('done');
            }
        },
        //Set column properties.
        "columnDefs": [
        { 
            "targets": [ 0,7], //first & last column
            "orderable": false, //set not orderable
        },
        ],
    });

     //datatable configuration
    deltable = $('#delmemberstable').DataTable({ 
        "processing": true,
        "serverSide": true,
        "searching": false,
        "order": [],
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "readdeleted.php",
            "type": "POST",
            data:{},
            complete: function () {
                // alert('done');
            }
        }
    });

     $("#btnhome").click(function () {
            $("#mem_list").show();
            $("#del_mem_list").hide();
     });
     $("#btndeleted").click(function () {
            $("#del_mem_list").show();
            $("#mem_list").hide();
     });
});
//refresh the datatable
function reload_member_list()
{
    table.ajax.reload(null,false); //reload datatable ajax 
    deltable.ajax.reload(null,false);
    $("#mem_list").show();
    $("#del_mem_list").hide();
}
//open modal popup for new member
function open_new_modal()
{
    save_mode="add";
    $('#addMemberForm')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#addMember_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add New Member'); // Set Title to Bootstrap modal title
}
//save member details - either add new or update the existing one
function save_member()
{
    var chk_err = validate_inputs();
    if(chk_err == false)
    {
        if(save_mode == "add")
        {
            var formurl = "addmember";
            var success_msg ="Member Added Successfully";
        }
        else
        {
            var formurl = "updatemember";
            var success_msg ="Member Details Updated Successfully";
        }
        
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable 

        // ajax function for adding data into database
        $.ajax({
            url : formurl,
            type: "POST",
            data: $('#addMemberForm').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                if(data.status) //if success close modal and refresh employee list
                {
                    $('#addMember_form').modal('hide'); //close modal popup
                    reload_member_list(); //refresh list
                    toastr.success(success_msg); //success message
                }
                else
                {
                    for (var i = 0; i < data.err_element.length; i++) 
                    {
                        $('[name="'+data.err_element[i]+'"]').parent().parent().addClass('has-error'); //select form-group and add has-error class
                        $('[name="'+data.err_element[i]+'"]').next().text(data.err_msg[i]); //select help-block class set error message
                    }
                }
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding data');
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 
     
            }
        });
      }
}
//view member detail in modal popup
function view_member(id)
{
    var url = "getmemberdetail";
 
    //Ajax Load data from ajax
    $.ajax({
        url : url,
        type: "POST",
        data:{'id':id},
        dataType: "JSON",
        success: function(data)
        {
            $('[name="firstname"]').text(data.firstname);
            $('[name="lastname"]').text(data.lastname);
            $('[name="email"]').text(data.email);
            $('[name="phone"]').text(data.telephone);
            $('[name="dob"]').text(data.dob);
            $('[name="address"]').text(data.address);
            $('[name="city"]').text(data.city);
            $('[name="province"]').text(data.province);
            $('[name="postalcode"]').text(data.postalcode);
            $('#viewMember').modal('show'); // show bootstrap modal
            $('.modal-title').text('Member Details'); // Set Title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}   
//get selected employee details & set in popup
function edit_member(id)
{
    save_mode = "update";
    var url = "getmemberdetail";
    $('#addMemberForm')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
 
    //Ajax Load data from ajax
    $.ajax({
        url : url,
        type: "POST",
        data:{'id':id},
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);
            $('[name="firstname"]').val(data.firstname);
            $('[name="lastname"]').val(data.lastname);
            $('[name="email"]').val(data.email);
            $('[name="phone"]').val(data.telephone);
            $('[name="dob"]').val(data.dob);
            $('[name="address"]').val(data.address);
            $('[name="city"]').val(data.city);
            $('[name="province"]').val(data.province);
            $('[name="postalcode"]').val(data.postalcode);
            $('#addMember_form').modal('show'); // show bootstrap modal
            $('.modal-title').text('Edit Member Details'); // Set Title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
//function to validate email
function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test( $email );
}
//validate inputs
function validate_inputs()
{
    var input_ele = ["firstname", "lastname", "email","phone", "dob", "address","city", "province", "postalcode"];
    var elen = input_ele.length;
    var has_err = false;
    for (i = 0; i < elen; i++) 
    {
      if($('[name="'+input_ele[i]+'"]').val().trim() == "")
      {
        
        $('[name="'+input_ele[i]+'"]').parent().parent().addClass('has-error'); //select form-group and add has-error class
        $('[name="'+input_ele[i]+'"]').next().text("Required");
        has_err = true;
      }
      else
      {
        if(input_ele[i] == "email")
        {
            if( !validateEmail($('[name="'+input_ele[i]+'"]').val().trim())) 
            {
                $('[name="'+input_ele[i]+'"]').parent().parent().addClass('has-error'); //select form-group and add has-error class
                $('[name="'+input_ele[i]+'"]').next().text("Invalid Email");
                has_err = true;
            }
            else
            {
              $('[name="'+input_ele[i]+'"]').parent().parent().removeClass('has-error'); //select form-group and add has-error class
              $('[name="'+input_ele[i]+'"]').next().text("");
            }
        }
        else
        {
            $('[name="'+input_ele[i]+'"]').parent().parent().removeClass('has-error'); //select form-group and add has-error class
            $('[name="'+input_ele[i]+'"]').next().text("");
        } 
      }
    }
    return has_err;
}
//delete member
function delete_member(id)
{
  if(confirm('Are you sure delete this member?'))
    {
        var url = "deletemember";
        $.ajax({
            url : url,
            type: "POST",
            data:{'id':id},
            dataType: "JSON",
            success: function(data)
            {
                reload_member_list(); //refresh list
                toastr.success('Member Deleted Successfully'); //success message
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
 
    }
}
