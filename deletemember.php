<?php
/**
   * delete member
   * 
   * delete selected member
   * 
   * @author  Parimal Naik
   */

require 'class.crud.php';
$obj = new CRUD();

$id = $_POST['id'];
$result = $obj->delete_member($id);
echo json_encode(array("status" => TRUE));
?>