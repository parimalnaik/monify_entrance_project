-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 13, 2017 at 11:38 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crud`
--

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `province` varchar(2) DEFAULT NULL,
  `postalcode` varchar(15) DEFAULT NULL,
  `isdeleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `lastname`, `firstname`, `email`, `telephone`, `dob`, `address`, `city`, `province`, `postalcode`, `isdeleted`) VALUES
(1, 'Naik', 'Parimal', 'parimalnaik@gmail.com', '514 619 1553', '1984-06-08', '311 Blvd Jules Poitras', 'Ville Saint Laurent', 'QC', 'H4N 1Y4', 0),
(2, 'Desai', 'Gira', 'gira@mail.com', '514-999-9999', '1984-11-14', '3380 Glover Road', 'Langley', 'BC', 'V3A 4P6', 0),
(3, 'Patel', 'Gaurang', 'gaurang@mail.com', '514-784-054', '1985-05-10', '1510 Yonge Street', 'Toronto', 'ON', 'M4W 1J7', 0),
(4, 'Desai', 'Darshan', 'darshan@mail.com', '514-080-0660', '1980-09-04', '2097 Leduc Street', 'Gracefield', 'QC', 'J0X 1W0', 1),
(5, 'Desai', 'Sita', 'sita@mail.com', '514-258-1259', '1950-05-06', '311 Jules Poitras', 'Saint Laurent', 'QC', 'H4N 1Y4', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
