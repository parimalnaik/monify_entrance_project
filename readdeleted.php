<?php
/**
   * list deleted member
   * 
   * list all deleted members in datatable
   * 
   * @author  Parimal Naik
   */
require 'class.crud.php';

$object = new CRUD();


$member = $object->deleted_members_List();

$data = array();
$no = @$_POST['start'];
foreach ($member as $members) {
    $no++;
    $row = array();
    $row[] = $no;
    $row[] = $members['firstname'];
    $row[] = $members['lastname'];
    $row[] = $members['email'];
    $row[] = $members['dob'];
    $row[] = $members['city'];
    $row[] = $members['province'];
    $data[] = $row;
}

$output = array(
                "draw" => @$_POST['draw'],
                "recordsTotal" => $object->del_count_all(),
                "recordsFiltered" => $object->del_count_selected(),
                "data" => $data,
        );
//output to json format
echo json_encode($output);
?>