<?php
/**
   * Databse Configuration file
   * 
   * configure database
   * 
   * @author  Parimal Naik
   */

// Database Connection variables
define('HOST', 'localhost'); // Host name
define('DBUSER', 'root'); // Username
define('DBPASSWORD', ''); // Password
define('DATABASE', 'crud'); // Database name

function DB()
{
    static $DB_con;

    try
    {
     $DB_con = new PDO("mysql:host=".HOST.";dbname=".DATABASE,DBUSER,DBPASSWORD);
     $DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
     $DB_con->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
     $DB_con->setAttribute(PDO::ATTR_EMULATE_PREPARES, FALSE);
    }
    catch(PDOException $e)
    {
     echo $e->getMessage();
    }

    return $DB_con;
}
?>