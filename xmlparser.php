<?php
/**
   * XML parser
   * 
   * Parse the local XML file and adds records into members table. Do the batch entries in table.
   * 
   * @author  Parimal Naik
   */

define('INSERT_BATCH_SIZE', 1000);
define('MEMBERS_XML_FILE', 'members.xml');

require 'class.crud.php';
$obj = new CRUD();

	$members = array();

    $xmlReader = new XMLReader();
    $xmlReader->open(MEMBERS_XML_FILE);

    // Move our pointer to the first <member /> element.
    while ($xmlReader->read() && $xmlReader->name !== 'member') ;

    $memberCount = 0;
    $totalMembers = 0;

    // Iterate over the outer <member /> elements.
    while ($xmlReader->name == 'member')
    {

        // Convert the node into a SimpleXMLElement for ease of use.
        $item = new SimpleXMLElement($xmlReader->readOuterXML());

        $id=$xmlReader->getAttribute('id');
        $firstname = $item->firstname;
        $lastname = $item->lastname;
        $email = $item->email;
        $telephone = $item->telephone;
        $dob = $item->dob;
        $address = $item->address;
        $city = $item->city;
        $province = $item->province;
        $postalcode = $item->{'postal-code'};

        $members[] = array($id,$firstname,$lastname,$email,$telephone,$dob,$address,$city,$province,$postalcode);
        $memberCount++;
        $totalMembers++;

        // Once we've reached the desired batch size, insert the batch and reset the counter.
        if ($memberCount >= INSERT_BATCH_SIZE)
        {
        	//var_dump($members);
            $obj->batchInsertMembers($members);
            $memberCount = 0;
            unset($members);
			$members = array();
        }

        // Go to next <member />.
        $xmlReader->next('member');
    }

    $xmlReader->close();

    // Insert the leftovers from the last batch.
    $obj->batchInsertMembers($members);

    echo "Inserted $totalMembers total members.";
?>