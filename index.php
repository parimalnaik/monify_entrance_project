<?php include_once 'header.php'; ?>
<!-- Content Section -->
    <div class="row" id="mem_list">
        <div class="col-md-12">
            <h3>Members:</h3>
            <table id="memberstable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>DOB</th>
                    <th>City</th>
                    <th>Province</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        </div>
    </div>
    <div class="row" id="del_mem_list" style="display:none;">
        <div class="col-md-12">
            <h3>Deleted Members:</h3>
            <table id="delmemberstable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>DOB</th>
                    <th>City</th>
                    <th>Province</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        </div>
    </div>
<?php include_once 'footer.php';?>